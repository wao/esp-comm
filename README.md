### Links
> [Electro Dragon](http://www.electrodragon.com/w/Wi07c)

> [NURD Space](https://nurdspace.nl/ESP8266)

> [Wi07c](http://www.electrodragon.com/w/Wi07c)


### Template Variable Format
> $(FieldName)[DataType]

* Number Field
  * $(Integer Name)[i]
  * $(Decimal Name)[d]
* String Field  
  * $(String Name)
  * $(String Name)[s]
  * s flag is optional/implied
* Choice Field
  * $(Choice Name, 1|2|3)[c]
  * $(Choice Name, 1=Choice1|2|3=Third Choice)[c]
  * $(Choice Name, Hello=World|2=King)[c]

### AT Command Config File
* &lt;AT COMMAND&gt;  &lt;at lease 2x Spaces or 1 Tab&gt; &lt;discription of the command&gt;
