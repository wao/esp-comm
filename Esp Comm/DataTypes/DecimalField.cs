﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LittleUmph;

namespace Esp_Comm.DataTypes
{
    public class DecimalField : DataField
    {
        public DecimalField(string fieldName) : base(fieldName)
        {
            FieldType = DataTypes.FieldType.Decimal;
        }

        public override bool IsValid()
        {
            if (base.IsValid())
            {
                var validDouble = Str.IsDouble(Text);
                if (!validDouble)
                {
                    lblErrorMsg.Text = "This sucker has to be a decimal number.";
                }
                return validDouble;
            }
            return false;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // cboChoice
            // 
            this.cboChoice.Size = new System.Drawing.Size(254, 30);
            // 
            // DecimalField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.Name = "DecimalField";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

    }
}
