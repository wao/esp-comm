﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using LittleUmph;

namespace Esp_Comm
{
    public class ATCommand
    {
        public string AT { get; set; }
        public string Template { get; set; }
        public string HelpText { get; set; }

        public bool Valid { get; protected set; }

        public ATCommand(string line)
        {
            line = line.Trim();
            var regex = new Regex(@"(\s{2,}|\t)+", RegexOptions.IgnoreCase | RegexOptions.Compiled);

            var splits = regex.Split(line);
            if (splits.Length > 0)
            {
                Template = splits[0].Trim();

                if (splits.Length == 3)
                {
                    HelpText = splits[2].Trim();
                }
                else if (splits.Length > 3)
                {
                    var list = new List<string>(splits);
                    list.RemoveAt(0);
                    list.RemoveAt(0);
                    HelpText = Arr.Implode(" ", list);
                }

                var atRegex = new Regex(@"\$\((.+?)(, .+?)?\)(\s*\[\w\])?");
                AT = atRegex.Replace(Template, "<$1>");
            }
            else
            {
                Console.WriteLine("");
            }

            Valid = Str.IsNotEmpty(AT) && Str.IsNotEmpty(Template);
        }
    }
}
