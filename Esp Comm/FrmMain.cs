﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using LittleUmph;

namespace Esp_Comm
{
    public partial class FrmMain : Form
    {
        const string STR_Default_AT_File = "ESP AT Commands.txt";
        Dictionary<string, ATCommand> _atCommands = new Dictionary<string, ATCommand>();
        List<string> _history = new List<string>();

        public FrmMain()
        {
            InitializeComponent();            
        }

        private FileInfo GetAtFile()
        {
            var filePath = Path.Combine(Properties.Settings.Default.AtCommandPath, Properties.Settings.Default.LastATSelection);
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath);
            }

            filePath = Path.Combine(Properties.Settings.Default.AtCommandPath, STR_Default_AT_File);
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath);
            }

            return null;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            RefreshComList();
            var atFilePath = GetAtFile();
            if (atFilePath != null && atFilePath.Exists)
            {
                ParseAtCommandList(atFilePath);
            }
        }

        private void ParseAtCommandList(FileInfo file)
        {
            Text = String.Format("ESP Comm - {0}", Path.GetFileNameWithoutExtension(file.FullName));
            Properties.Settings.Default.LastATSelection = file.Name;
            Properties.Settings.Default.Save();

            ltCommands.Items.Clear();
            
            if (file.Exists)
            {
                var lines = File.ReadAllLines(file.FullName);

                foreach (var l in lines)
                {
                    if (l.StartsWith("#"))
                    {
                        continue;
                    }

                    if (Str.IsEmpty(l))
                    {
                        continue;
                    }


                    var at = new ATCommand(l);
                    if (at.Valid)
                    {
                        _atCommands[at.AT] = at;
                        ltCommands.Items.Add(at.AT);
                    }

                }
            }
        }

        private void RefreshComList()
        {
            cboCom.Items.Clear();
            var ports = SerialPort.GetPortNames();
            foreach (var p in ports)
            {
                cboCom.Items.Add(p);
            }
        }

        private void btnParse_Click(object sender, EventArgs e)
        {
            var frm = new FrmInput(settingSaver, txtCommand.Text);
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
                txtCommand.Text = frm.GetResult();
                settingSaver.DataStore.RefreshData();

                if (chkSendAfterParse.Checked)
                {
                    btnSend_Click(btnSend, e);
                }
            }
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var buffer = serialPort.ReadExisting();

            Invoke(new Action(() => {
                txtReply.Text += buffer;
                UI.ScrollToEnd(txtReply);
            }));
        }

        private void serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            Dlgt.Invoke(txtReply, () =>
            {
                txtReply.Text += "Error: " + e.ToString();
            });
        }

        private void ltCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ltCommands.SelectedIndex != -1)
            {
                var selected = ltCommands.SelectedItem.ToString();

                if (_atCommands.ContainsKey(selected))
                {
                    var at = _atCommands[selected];

                    txtCommand.Text = at.Template;
                    lblDescription.Text = at.HelpText;
                }
            }

        }

        private void SetToolTip(object sender, EventArgs e)
        {
            var c = (Control)sender;
            toolTip.SetToolTip(c, c.Text);

            if (c == txtCommand)
            {
                btnParse.Enabled = btnSend.Enabled = Str.IsNotEmpty(txtCommand.Text);
                btnParse.Enabled = Str.Contains(txtCommand.Text, "$");
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.Text == "Open")
            {
                try
                {
                    serialPort.PortName = cboCom.Text;
                    serialPort.BaudRate = Str.IntVal(cboBaud.SelectedItem.ToString());

                    serialPort.Open();

                    if (serialPort.IsOpen)
                    {
                        btnConnect.Text = "Close";
                        cboCom.Enabled = cboBaud.Enabled = false;

                        txtReply.Text += "Serial opened!\r\n";
                    }
                    else
                    {
                        txtReply.Text += "Something woong!\r\n";
                    }
                }
                catch (Exception xp)
                {
                    LogError(xp.Message);
                }
            }
            else
            {
                try
                {
                    btnConnect.Text = "Open";
                    cboCom.Enabled = cboBaud.Enabled = true;
                    serialPort.Close();
                }
                catch (Exception xp)
                {
                    LogError(xp.Message);
                }
            }
        }

        private void LogError(string errorMessage)
        {
            txtReply.Text += "Error: " + errorMessage + "\r\n";
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkClearB4Send.Checked)
                {
                    txtReply.Clear();
                }

                AddToHistory(txtCommand.Text);
                var cmd = txtCommand.Text.Replace("\\r\\n", "\r\n");
                serialPort.Write(cmd + "\r\n");
            }
            catch (Exception xp)
            {
                LogError(xp.Message);
            }
        }

        private void AddToHistory(string command)
        {
            if (Str.IsEmpty(command))
            {
                return;
            }

            if (_history.Contains(command))
            {
                _history.Remove(command);
            }
            _history.Insert(0, command);

            if (_history.Count > 7)
            {
                _history.RemoveAt(_history.Count - 1);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtReply.Clear();
        }
        
        private void btnHistory_Click(object sender, EventArgs e)
        {
            ctxHistory.Items.Clear();
            foreach (var h in _history)
            {
                var itm = new ToolStripMenuItem(h, null, (s, evt) =>
                {
                    txtCommand.Text = ((ToolStripMenuItem)s).Text;
                });
                ctxHistory.Items.Add(itm);
            }

            UI.ShowContext(ctxHistory, btnHistory);
        }

        private void ltCommands_DoubleClick(object sender, EventArgs e)
        {
            ltCommands_SelectedIndexChanged(sender, e);
            if (txtCommand.Text.Contains("$"))
            {
                if (btnParse.Enabled)
                {
                    btnParse_Click(btnParse, EventArgs.Empty);
                }
            }
            else
            {
                btnSend_Click(sender, e);
            }
        }

        private void cboCom_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnConnect.Enabled = Str.IsNotEmpty(cboCom.Text);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshComList();
        }

        private void lnSwitchAt_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            var frm = new FrmATSelection();
            var result = frm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                if (frm.ATFile != null)
                {
                    ParseAtCommandList(frm.ATFile);
                }
            }
        }
    }
}
